image: gcr.io/google.com/cloudsdktool/cloud-sdk

# include sast engine
include:
  project: amfament/ent/tanuki/microfocus/sast-include
  file: /sast.yaml

stages:
  - build
  - deploy
  - smoke-test
  - sast-scan

variables:
  DOCKER_DRIVER: overlay2
  DOCKER_HOST: "tcp://docker:2376"
  DOCKER_TLS_VERIFY: "1"
  DOCKER_TLS_CERTDIR: "/certs"
  DOCKER_CERT_PATH: "/certs/client"
  REGION: us-east4
  PROJECT_NAME: "edds-lite-outbound"
  SVC_ACCOUNT_PREFIX: "edds-lite-outbound@gcp-ent-edds"
  SVC_ACCOUNT_SUFFIX: "iam.gserviceaccount.com"
  ENDPOINT_SVC_ACCOUNT_PREFIX: "edds-lite-outbound-endpoint@gcp-ent-edds"
  ENDPOINT_SVC_ACCOUNT_SUFFIX: "iam.gserviceaccount.com"
  GCP_PROJECT_PREFIX: "gcp-ent-edds"
  MEMORY: 2Gi

# services:
#   - docker:dind

.deploy-commit-ref-slug:
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
    GCP_PROJECT: "$GCP_PROJECT_PREFIX-$CI_ENVIRONMENT_NAME"
    SVC_ACCOUNT: "$SVC_ACCOUNT_PREFIX-$CI_ENVIRONMENT_NAME.$SVC_ACCOUNT_SUFFIX"
    ENDPOINT_SVC_ACCOUNT: "$ENDPOINT_SVC_ACCOUNT_PREFIX-$CI_ENVIRONMENT_NAME.$ENDPOINT_SVC_ACCOUNT_SUFFIX"
  script:
    - echo "gcloud auth configure-docker --quiet"
    - echo "docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY"
    - echo "docker pull $IMAGE_TAG"
    - echo "docker tag \"$IMAGE_TAG gcr.io/$GCP_PROJECT/$PROJECT_NAME-$CI_COMMIT_REF_SLUG\""
    - echo "docker push \"gcr.io/$GCP_PROJECT/$PROJECT_NAME-$CI_COMMIT_REF_SLUG\""
    - >
      echo "gcloud run deploy $PROJECT_NAME-$CI_COMMIT_REF_SLUG
      --region $REGION
      --project $GCP_PROJECT
      --set-env-vars EXECUTION_ENV=$CI_ENVIRONMENT_NAME
      --image gcr.io/$GCP_PROJECT/$PROJECT_NAME-$CI_COMMIT_REF_SLUG
      --platform managed
      --service-account $SVC_ACCOUNT
      --memory $MEMORY
      --allow-unauthenticated"
    - >
      echo "gcloud run services add-iam-policy-binding $PROJECT_NAME-$CI_COMMIT_REF_SLUG
      --member=\"group:gcp-ent-edds-data-engineer-gs@amfam.com\"
      --role=\"roles/run.invoker\"
      --platform managed
      --region $REGION"

# TODO: This should no longer be latest. Each pipeline needs to be tied to a tag/commit-sha.
# TODO: Add rollback?
.deploy-latest:
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:latest
    GCP_PROJECT: "$GCP_PROJECT_PREFIX-$CI_ENVIRONMENT_NAME"
    SVC_ACCOUNT: "$SVC_ACCOUNT_PREFIX-$CI_ENVIRONMENT_NAME.$SVC_ACCOUNT_SUFFIX"
    ENDPOINT_SVC_ACCOUNT: "$ENDPOINT_SVC_ACCOUNT_PREFIX-$CI_ENVIRONMENT_NAME.$ENDPOINT_SVC_ACCOUNT_SUFFIX"
  script:
    - echo "gcloud auth configure-docker --quiet"
    - echo "docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY"
    - echo "docker pull $IMAGE_TAG"
    - echo "docker tag $IMAGE_TAG \"gcr.io/$GCP_PROJECT/$PROJECT_NAME\""
    - echo "docker push \"gcr.io/$GCP_PROJECT/$PROJECT_NAME\""
    - >
      echo "gcloud run deploy $PROJECT_NAME
      --region $REGION
      --project $GCP_PROJECT
      --set-env-vars EXECUTION_ENV=$CI_ENVIRONMENT_NAME
      --image gcr.io/$GCP_PROJECT/$PROJECT_NAME
      --platform managed
      --service-account $SVC_ACCOUNT
      --memory $MEMORY"
    # TODO: update --member below to a group of developers
    - >
      echo "gcloud run services add-iam-policy-binding $PROJECT_NAME
      --member=\"serviceAccount:$ENDPOINT_SVC_ACCOUNT\"
      --role=\"roles/run.invoker\"
      --platform managed
      --region $REGION"



.deploy-latest-test:
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:latest
    GCP_PROJECT: "$GCP_PROJECT_PREFIX-$CI_ENVIRONMENT_NAME"
    SVC_ACCOUNT: "$SVC_ACCOUNT_PREFIX-$CI_ENVIRONMENT_NAME.$SVC_ACCOUNT_SUFFIX"
    ENDPOINT_SVC_ACCOUNT: "$ENDPOINT_SVC_ACCOUNT_PREFIX-$CI_ENVIRONMENT_NAME.$ENDPOINT_SVC_ACCOUNT_SUFFIX"
  script:
    - echo "gcloud auth configure-docker --quiet"
    - echo "docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY"
    - echo "docker pull $IMAGE_TAG"
    - echo "docker tag $IMAGE_TAG \"gcr.io/$GCP_PROJECT/$PROJECT_NAME-$CI_COMMIT_SHORT_SHA\""
    - echo "docker push \"gcr.io/$GCP_PROJECT/$PROJECT_NAME-$CI_COMMIT_SHORT_SHA\""
    - >
      echo "gcloud run deploy $PROJECT_NAME-$CI_COMMIT_SHORT_SHA
      --region $REGION
      --project $GCP_PROJECT
      --set-env-vars EXECUTION_ENV=$CI_ENVIRONMENT_NAME
      --image gcr.io/$GCP_PROJECT/$PROJECT_NAME-$CI_COMMIT_SHORT_SHA
      --platform managed
      --service-account $SVC_ACCOUNT
      --memory $MEMORY"
    - >
      echo "gcloud run services add-iam-policy-binding $PROJECT_NAME-$CI_COMMIT_SHORT_SHA
      --member=\"user:tnguye1@amfam.com\"
      --role=\"roles/run.invoker\"
      --platform managed
      --region $REGION"

.smoke-test:
  script:
    - apt-get -y install nodejs
    - apt-get -y install npm
    - npm install -g newman
    - newman --version
    - npm install -g newman-reporter-html
    - >
      newman run src/test/resources/edds-lite-outbound-query-api-smoke-test.postman_collection.json
      -e $POSTMAN_ENVIRONMENT_FILE
      --reporters cli,html,junit
      --reporter-html-export report.html
      --reporter-junit-export report.xml
  artifacts:
    when: always
    paths:
      - report.html
    reports:
      junit: report.xml

# TODO: create commit-sha cleanup stage to delete cloud run instances when done
build-branch:
  stage: build
  # tags:
  #   - dev
  #   - gitlab-org-docker
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  script:
    - echo "docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY"
    - echo "DOCKER_BUILDKIT=1 docker build --pull -t $IMAGE_TAG ."
    - echo "docker push $IMAGE_TAG"
  except:
    - master

deploy-branch:
  extends: .deploy-commit-ref-slug
  environment:
    name: dev
  stage: deploy
  needs: [build-branch]
  # tags:
  #   - dev
  #   - gitlab-org-docker
  except:
    - master

build-master:
  stage: build
  # tags:
  #   - dev
  #   - gitlab-org-docker
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:latest
  script:
    - echo "docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY"
    - echo "DOCKER_BUILDKIT=1 docker build --pull -t $IMAGE_TAG ."
    - echo "docker push $IMAGE_TAG"
  only:
    - master

deploy-master-to-dev:
  extends: .deploy-latest
  environment:
    name: dev
  stage: deploy
  needs: [build-master]
  # tags:
  #   - dev
  #   - gitlab-org-docker
  only:
    - master

deploy-master-to-preprod:
  extends: .deploy-latest
  environment:
    name: preprod
  stage: deploy
  needs: [build-master]
  # tags:
  #   - preprod
  #   - gitlab-org-docker
  only:
    - master
  when: manual

test-deploy-master-to-preprod:
  extends: .deploy-latest-test
  environment:
    name: preprod
  stage: deploy
  needs: [build-master]
  # tags:
  #   - preprod
  #   - gitlab-org-docker
  only:
    - master
  when: manual

deploy-master-to-prod:
  extends: .deploy-latest
  environment:
    name: prod
  stage: deploy
  needs: [build-master]
  # tags:
  #   - prod
  #   - gitlab-org-docker
  only:
    - master
  when: manual

smoke-test-dev:
  extends: .smoke-test
  stage: smoke-test
  needs: [deploy-master-to-dev]
  environment:
    name: dev
  # tags:
  #   - dev
  #   - gitlab-org-docker
  only:
    - master

smoke-test-preprod:
  extends: .smoke-test
  stage: smoke-test
  needs: [deploy-master-to-preprod]
  environment:
    name: preprod
  # tags:
  #   - preprod
  #   - gitlab-org-docker
  only:
    - master

smoke-test-prod:
  extends: .smoke-test
  stage: smoke-test
  needs: [deploy-master-to-prod]
  environment:
    name: prod
  # tags:
  #   - prod
  #   - gitlab-org-docker
  only:
    - master

sast-scan:
  stage: sast-scan
  extends: .sast
  variables:
    JDK_DOWNLOAD_URL: https://github.com/AdoptOpenJDK/openjdk11-upstream-binaries/releases/download/jdk-11.0.10%2B9/OpenJDK11U-jdk_x64_linux_11.0.10_9.tar.gz
    JAVA_PATH: openjdk-11.0.10_9
  when: manual
  # tags:
  #   - gcp
  #   - gitlab-org-docker
